/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:23:39 by diuchi            #+#    #+#             */
/*   Updated: 2020/11/15 15:07:15 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*res;
	int		i;
	int		l;
	int		charset[300];

	if (!(s1 && set))
		return (NULL);
	i = 0;
	while (i < 300)
		charset[i++] = 0;
	while (*set)
		charset[(int)*set++]++;
	while (*s1 && charset[(int)*s1])
		s1++;
	l = ft_strlen(s1);
	while (l && charset[(int)s1[l - 1]])
		l--;
	if (!(res = (char *)malloc(sizeof(char) * (l + 1))))
		return (NULL);
	ft_strlcpy(res, s1, l + 1);
	return (res);
}
