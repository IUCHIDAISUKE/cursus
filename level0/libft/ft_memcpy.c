/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/10 16:35:58 by diuchi            #+#    #+#             */
/*   Updated: 2020/10/19 12:46:48 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char		*res;
	const char	*sr;

	res = (char *)dest;
	sr = (const char *)src;
	if (!(dest || src))
		return (NULL);
	while (n--)
	{
		*res = *sr;
		res++;
		sr++;
	}
	return (dest);
}
