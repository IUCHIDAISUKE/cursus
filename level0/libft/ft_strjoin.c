/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:11:50 by diuchi            #+#    #+#             */
/*   Updated: 2021/01/23 15:39:20 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

// char	*ft_strjoin(char const *s1, char const *s2)
// {
// 	char *res;

// 	if (!(s1 || s2))
// 		return (ft_strdup(""));
// 	if (!s1 || !s2)
// 		return (!s1 ? (ft_strdup(s2)) : ft_strdup(s1));
// 	if (!(res = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1)))
// 		return (NULL);
// 	ft_strlcpy(res, s1, ft_strlen(s1) + 1);
// 	ft_strlcat(res, (char *)s2, ft_strlen(s1) + ft_strlen(s2) + 1);
// 	return (res);
// }

char *ft_strjoin(char const *s1, char const *s2)
{
	char *res;
	int size1;
	int size2;

	if (!(s1 || s2))
		return ((char *)NULL);
	if (!s1 || !s2)
		return (!s1 ? (char *)s2 : (char *)s1);
	size1 = ft_strlen(s1);
	size2 = ft_strlen(s2);
	if (!(res = (char *)malloc(size1 + size2 + 1)))
		return (NULL);
	*res = 0;
	ft_strlcat(res, (char *)s1, size1 + 1);
	ft_strlcat(res, (char *)s2, size1 + size2 + 1);
	return (res);
}

// realloc()

// #include <string.h>
// #include <stdio.h>
// int main(){

// 	char *s1 = "where is my ";
// 	char *s2 = "malloc ???";
// 	char *s3 = "where is my malloc ???";

// 	char *res = ft_strjoin(s1, s2);
// 	printf("%d", *res);
// 	printf("%d", 'w');
// 	// for (int i = 0; i < (int)ft_strlen(res) + 1; i++)
// 	// {
// 	// 	printf("%d\n",res[i]);
// 	// }
// 	// int n = strcmp(s3, res);
// 	// if (!n)
// 	// {
// 	// 	printf("ok");
// 	// }
// 	// printf("no");
// }
