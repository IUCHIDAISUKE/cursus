/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 01:14:55 by diuchi            #+#    #+#             */
/*   Updated: 2020/10/25 11:09:56 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	int i;

	if (!*needle)
		return ((char *)haystack);
	if (ft_strlen(haystack) < ft_strlen(needle))
		return (NULL);
	i = 0;
	while (haystack[i] && i + (int)ft_strlen(needle) <= (int)len)
	{
		if(!ft_strncmp(haystack + i, needle, ft_strlen(needle)))
			return ((char *)&haystack[i]);
		i++;
	}
	return (NULL);
}
