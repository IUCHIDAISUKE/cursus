/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:18:14 by diuchi            #+#    #+#             */
/*   Updated: 2020/10/25 10:53:12 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	long	tmp;
	int		size;
	char	*ascii;

	size = 1;
	tmp = n;
	while (n /= 10)
		size++;
	size += ((tmp < 0) ? 1 : 0);
	if (!(ascii = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	ascii[size--] = '\0';
	if (tmp == 0)
		ascii[0] = '0';
	if (tmp < 0)
	{
		ascii[0] = '-';
		tmp *= -1;
	}
	while (tmp)
	{
		ascii[size--] = tmp % 10 + '0';
		tmp /= 10;
	}
	return (ascii);
}
