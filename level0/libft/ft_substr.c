/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/09 17:10:46 by diuchi            #+#    #+#             */
/*   Updated: 2020/10/25 11:37:38 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	size;
	char	*res;

	if (s == NULL)
		return (NULL);
	if (ft_strlen(s) < start)
		size = 0;
	else
		if (ft_strlen(s) - start < len)
			size = ft_strlen(s) - start;
		else
			size = len;
	// size = ((ft_strlen(s) < start) ? 0 : ft_strlen(s) - start);
	// size = ((ft_strlen(s) - start < len) ? size : len);
	if (!(res = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	ft_strlcpy(res, s + start, size + 1);
	return (res);
}
