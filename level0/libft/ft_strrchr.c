/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 11:26:05 by diuchi            #+#    #+#             */
/*   Updated: 2020/11/01 01:46:46 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*res="";

	i = 0;
	res = NULL;
	while (i < (int)ft_strlen(s) + 1)
	{
		if (s[i] == c)
			res = (char *)&s[i];
		i++;
	}
	return (res);
}
