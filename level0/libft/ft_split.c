/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 10:41:00 by diuchi            #+#    #+#             */
/*   Updated: 2021/01/23 18:15:05 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

// static char	**comp_free(char **strset, int ma)
// {
// 	int i;

// 	i = 0;
// 	while (i <= ma)
// 	{
// 		free(strset[i]);
// 		strset[i] = NULL;
// 		i++;
// 	}
// 	free(strset);
// 	strset = NULL;
// 	return (strset);
// }

// char		**ft_split(char const *s, char c)
// {
// 	char	**strs;
// 	int		i;

// 	if (!s || !(strs = (char **)malloc(sizeof(char *) * ((ft_strlen(s) + 1)))))
// 		return (NULL);
// 	i = 0;
// 	while (*s)
// 	{
// 		while (*s == c)
// 			s++;
// 		if (!(ft_strlen(s)))
// 			break ;
// 		if (!(strs[i] = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1))))
// 			return (strs = comp_free(strs, i));
// 		if (!(ft_strchr(s, c)))
// 			ft_strlcpy(strs[i], s, ft_strlen(s) + 1);
// 		else
// 			ft_strlcpy(strs[i], s, ft_strchr(s, c) - s + 1);
// 		s += ft_strlen(strs[i++]);
// 	}
// 	strs[i] = NULL;
// 	return (strs);
// }

#include "libft.h"
#include <stdlib.h>

static int strbycnt(char const *s, char c)
{
	size_t i;
	int id;

	id = 0;
	if (!ft_strlen(s))
		return (0);
	if (s[0] != c)
		id++;
	i = 0;
	while (s[i + 1])
	{
		if (s[i] == c && s[i + 1] != c)
			id++;
		i++;
	}
	return (id);
}

static char **ft_all_free(char **p)
{
	int j;

	j = 0;
	while (p[j])
	{
		free(p[j]);
		j++;
	}
	free(p);
	return (NULL);
}

char **ft_split(char const *s, char c)
{
	char **p;
	int i;
	int len;

	if (!s || !(p = (char **)malloc(sizeof(char *) * (strbycnt(s, c) + 1))))
		return (NULL);
	i = 0;
	while (*s)
	{
		len = 0;
		while (*s == c)
			s++;
		while (s[len] && s[len] != c)
			len++;
		if (*s)
		{
			if (!(p[i] = (char *)malloc(sizeof(char) * (len + 1))))
				return (ft_all_free(p));
			ft_strlcpy(p[i], s, len + 1);
			s += len;
			i++;
		}
	}
	p[i] = NULL;
	return (p);
}

#include <stdio.h>

int count_splitelem(char **strs)
{
	int i;

	i = 0;
	if (!strs)
		return (i);
	if (!strs[i])
		return (i);
	while (strs[i])
	{
		i++;
	}
	return (i);
}

int main(){
	char test[] = "R 1234 235";
	int i = 0;
	char **strs = ft_split(test, ' ');

	int len = count_splitelem(strs);
	printf("len    : %d\n", len);
	printf("strs   : pointer=%p\n", strs);
	while (strs[i])
	{
		printf("str[%d] : pointer=%p : str=%s\n", i, strs[i], strs[i]);
		i++;
	}
	printf("str[%d] : pointer=%p : str=%s\n", i, strs[i], strs[i]);

	// (void)t;
}