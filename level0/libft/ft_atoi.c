/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 16:48:56 by diuchi            #+#    #+#             */
/*   Updated: 2020/10/25 10:52:33 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdbool.h>

int	ft_atoi(const char *str)
{
	bool minus;
	long num;

	while (('\t' <= *str && *str <= '\r') || *str == ' ')
		str++;
	minus = ((*str == '-') ? true : false);
	if (*str == '-' || *str == '+')
		str++;
	num = 0;
	while ('0' <= *str && *str <= '9')
	{
		num = 10 * num + (*str - '0') * (minus ? -1 : 1);
		str++;
		if (ft_isdigit(*str))
		{
			num = (LONG_MAX / 10 < num ? LONG_MAX : num);
			num = (LONG_MIN / 10 > num ? LONG_MIN : num);
			num = (LONG_MAX / 10 == num && (*str - '0') > 7 ? LONG_MAX : num);
			num = (LONG_MIN / 10 == num && (*str - '0') > 8 ? LONG_MIN : num);
		}
		if (num == LONG_MAX || num == LONG_MIN)
			break ;
	}
	return ((int)num);
}
