/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversion.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/20 14:19:32 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 14:49:03 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_CONVERSION_H
# define FT_CONVERSION_H

# include "../ft_printf.h"
# include <stdint.h>
# include <stdarg.h>
# include <stdbool.h>

int32_t ft_conversion_c(va_list *ap);
int32_t ft_conversion_str(va_list *ap);
int32_t ft_conversion_ptr_zero();
int32_t ft_conversion_ptr(va_list *ap);
int32_t ft_conversion_percent(void);
int32_t ft_conversion_decimal_sub(char *p);
int32_t ft_conversion_decimal(va_list *ap, bool unsign);
int32_t ft_conversion_hexadecimal_sub(char *p);
int32_t ft_conversion_hexadecimal(va_list *ap, bool upper);

#endif
