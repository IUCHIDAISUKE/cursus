/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_radix.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:16:34 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:17:11 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_RADIX_H
# define FT_RADIX_H

# include <unistd.h>
# include <stdint.h>
# include <stdlib.h>
# include <stdbool.h>
# include "../ft_printf.h"
# define RADIX "0123456789abcdef"

void ft_decimal(int64_t n, char *p);
void ft_hexadecimal(uint64_t n, bool upper, bool ptr, char *p);
void ft_pointer(void *vptr, char *p);

#endif
