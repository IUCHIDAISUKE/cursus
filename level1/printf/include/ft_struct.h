/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_struct.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:12:53 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 10:49:28 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRUCT_H
# define FT_STRUCT_H

# include <stdbool.h>
# include <stdint.h>

typedef struct s_data	t_data;
struct	s_data
{
	int64_t				flag;
	int64_t				conv;
	int64_t				wid;
	int64_t				prec;
	char				p[1200];
	int64_t				num;
	int64_t				digit;
};

void	ft_init(t_data *g_data);
void	ft_infoshow(t_data *g_data);

#endif
