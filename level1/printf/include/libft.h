/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/21 00:20:34 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:17:29 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stddef.h>
# include <stdlib.h>
# include <unistd.h>
# include <limits.h>
# include <stdbool.h>
# include <stdint.h>

typedef struct		s_list
{
	void			*content;
	struct s_list	*next;
}					t_list;

int32_t				ft_atoi(const char *str);
void				ft_bzero(void *s, uint64_t n);
void				*ft_calloc(uint64_t count, uint64_t size);
int32_t				ft_isalnum(int32_t c);
int32_t				ft_isalpha(int32_t c);
int32_t				ft_isascii(int32_t c);
int32_t				ft_isdigit(int32_t c);
int32_t				ft_isprint(int32_t c);
char				*ft_itoa(int32_t n);
void				ft_lstadd_back(t_list **lst, t_list *new);
void				ft_lstadd_front(t_list **lst, t_list *new);
void				ft_lstclear(t_list **lst, void (*del)(void *));
void				ft_lstdelone(t_list *lst, void (*del)(void *));
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstlast(t_list *lst);
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *), \
														void (*del)(void *));
t_list				*ft_lstnew(void *content);
int32_t				ft_lstsize(t_list *lst);
void				*ft_memccpy(void *dest, const void *src, int32_t c, \
																	uint64_t n);
void				*ft_memchr(const void *s, int32_t c, uint64_t n);
int32_t				ft_memcmp(const void *s1, const void *s2, uint64_t n);
void				*ft_memcpy(void *dest, const void *src, uint64_t n);
void				*ft_memmove(void *dest, const void *src, uint64_t n);
void				*ft_memset(void *buf, int32_t ch, uint64_t n);
int32_t				ft_putchar_fd(char c, int32_t fd);
int32_t				ft_putendl_fd(char *s, int32_t fd);
int32_t				ft_putnbr_fd(int32_t n, int32_t fd);
int32_t				ft_putstr_fd(char *s, int32_t fd);
char				**ft_split(char const *s, char c);
char				*ft_strchr(const char *s, int32_t c);
char				*ft_strdup(const char *s1);
char				*ft_strjoin(char const *s1, char const *s2);
uint64_t			ft_strlcat(char *dest, char *src, uint64_t size);
uint64_t			ft_strlcpy(char *dest, const char *src, uint64_t n);
uint64_t			ft_strlen(const char *s);
char				*ft_strmapi(char const *s, \
										char (*f)(uint32_t num, char c));
int32_t				ft_strncmp(const char *s1, const char *s2, \
																	uint32_t n);
char				*ft_strnstr(const char *big, const char *little, \
																uint64_t len);
char				*ft_strrchr(const char *s, int32_t c);
char				*ft_strtrim(char const *s1, char const *set);
char				*ft_substr(char const *s, uint32_t start, uint64_t len);
int32_t				ft_tolower(int32_t c);
int32_t				ft_toupper(int32_t c);

#endif
