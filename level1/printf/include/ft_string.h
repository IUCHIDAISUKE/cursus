/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:16:40 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:16:46 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRING_H
# define FT_STRING_H

# include "../ft_printf.h"
# include <stdint.h>

int32_t ft_strpos(const char *str, char c);

#endif
