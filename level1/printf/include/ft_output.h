/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:16:00 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:16:08 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_OUTPUT_H
# define FT_OUTPUT_H

# include <stdint.h>
# include <unistd.h>

int32_t ft_output_blank_fd(int64_t size, char c, int32_t fd);
int32_t ft_output_lenstr_fd(char *s, int32_t strlen, int32_t fd);
int32_t ft_output_number_fd(char p[], int32_t fd);
int32_t ft_output_str_fd(const char **fmt, int32_t fd);
#endif
