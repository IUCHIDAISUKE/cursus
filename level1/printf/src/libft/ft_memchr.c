/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/10 19:53:51 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:49:23 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

void	*ft_memchr(const void *s, int32_t c, uint64_t n)
{
	char	*p;

	p = (char *)s;
	while (n--)
	{
		if (*p == (char)c)
			return (p);
		p++;
	}
	return (NULL);
}
