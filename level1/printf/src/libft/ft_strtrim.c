/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:23:39 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:53:06 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*res;
	int32_t	i;
	int32_t	l;
	int32_t	charset[300];

	if (!(s1 && set))
		return (NULL);
	i = 0;
	while (i < 300)
		charset[i++] = 0;
	while (*set)
		charset[(int32_t)*set++]++;
	while (*s1 && charset[(int32_t)*s1])
		s1++;
	l = ft_strlen(s1);
	while (l && charset[(int32_t)s1[l - 1]])
		l--;
	if (!(res = (char *)malloc(sizeof(char) * (l + 1))))
		return (NULL);
	ft_strlcpy(res, s1, l + 1);
	return (res);
}
