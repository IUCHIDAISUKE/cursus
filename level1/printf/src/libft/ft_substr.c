/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/09 17:10:46 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:57:36 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

char	*ft_substr(char const *s, uint32_t start, uint64_t len)
{
	uint64_t	size;
	char		*res;

	if (s == NULL)
		return (NULL);
	if (ft_strlen(s) < start)
		size = 0;
	else
	{
		if (ft_strlen(s) < len + start)
			size = ft_strlen(s) - start;
		else
			size = len;
	}
	if (!(res = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	ft_strlcpy(res, s + start, size + 1);
	return (res);
}
