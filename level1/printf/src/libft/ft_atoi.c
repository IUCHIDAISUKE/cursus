/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 16:48:56 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:48:01 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

int32_t	ft_atoi(const char *str)
{
	bool	minus;
	int64_t num;

	while (('\t' <= *str && *str <= '\r') || *str == ' ')
		str++;
	minus = ((*str == '-') ? true : false);
	if (*str == '-' || *str == '+')
		str++;
	num = 0;
	while ('0' <= *str && *str <= '9')
	{
		num = 10 * num + (*str - '0') * (minus ? -1 : 1);
		str++;
		if (ft_isdigit(*str))
		{
			num = (INT64_MAX / 10 < num ? INT64_MAX : num);
			num = (INT64_MIN / 10 > num ? INT64_MIN : num);
			num = (INT64_MAX / 10 == num && (*str - '0') > 7 ? INT64_MAX : num);
			num = (INT64_MIN / 10 == num && (*str - '0') > 8 ? INT64_MIN : num);
		}
		if (num == INT64_MAX || num == INT64_MIN)
			break ;
	}
	return ((int32_t)num);
}
