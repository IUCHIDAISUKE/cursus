/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:08:02 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:20:08 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

int32_t	ft_putnbr_fd(int32_t n, int32_t fd)
{
	int32_t len;
	int32_t tmp;

	len = 1;
	tmp = n;
	while (tmp /= 10)
		len++;
	len += (n < 0 ? 1 : 0);
	if (n == INT32_MIN)
		return (write(fd, "-2147483648", 11));
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		n *= (-1);
		ft_putnbr_fd(n, fd);
	}
	else
	{
		if (n > 9)
			ft_putnbr_fd(n / 10, fd);
		ft_putchar_fd((n % 10) + '0', fd);
	}
	return (len);
}
