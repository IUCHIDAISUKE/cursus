/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 11:51:17 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:50:57 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

char		*ft_strchr(const char *s, int32_t c)
{
	int32_t	i;
	char	*res;

	i = 0;
	res = NULL;
	while (i < (int32_t)ft_strlen(s) + 1)
	{
		if (s[i] == c)
		{
			res = (char *)&s[i];
			break ;
		}
		i++;
	}
	return (res);
}
