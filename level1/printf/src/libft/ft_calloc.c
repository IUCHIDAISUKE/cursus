/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 01:16:02 by diuchi            #+#    #+#             */
/*   Updated: 2021/01/06 22:48:38 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

void	*ft_calloc(uint64_t count, uint64_t size)
{
	void *res;

	if (!(res = malloc(size * count)))
		return (NULL);
	ft_memset(res, 0, size * count);
	return (res);
}
