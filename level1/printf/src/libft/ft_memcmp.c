/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/10 21:44:22 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:49:31 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

int32_t	ft_memcmp(const void *s1, const void *s2, uint64_t n)
{
	const char *dst;
	const char *src;

	dst = (const char *)s1;
	src = (const char *)s2;
	while (n-- && (dst || src))
	{
		if (*dst != *src)
			return (*dst - *src);
		dst++;
		src++;
	}
	return (0);
}
