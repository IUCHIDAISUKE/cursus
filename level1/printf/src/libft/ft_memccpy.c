/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/06 16:26:17 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:49:16 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

void	*ft_memccpy(void *dst, const void *src, int32_t c, uint64_t n)
{
	char		*res;
	const char	*sr;

	res = (char *)dst;
	sr = (char *)src;
	while (n--)
	{
		*res = *sr;
		if (*sr == (char)c)
			return (res + 1);
		res++;
		sr++;
	}
	return (NULL);
}
