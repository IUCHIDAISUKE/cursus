/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:11:50 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/25 21:51:43 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char *res;

	if (!(s1 || s2))
		return (ft_strdup((char *)""));
	if (!s1 || !s2)
		return (!s1 ? (ft_strdup(s2)) : ft_strdup(s1));
	if (!(res = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1)))
		return (NULL);
	ft_strlcpy(res, s1, ft_strlen(s1) + 1);
	ft_strlcat(res, (char *)s2, ft_strlen(s1) + ft_strlen(s2) + 1);
	return (res);
}
