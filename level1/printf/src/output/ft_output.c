/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_output.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:21:16 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:21:47 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_output.h"

int32_t	ft_output_blank_fd(int64_t size, char c, int32_t fd)
{
	int64_t res;

	if (!c || size < 0)
		return (0);
	res = size;
	while (size--)
		write(fd, &c, 1);
	return (res);
}

int32_t	ft_output_lenstr_fd(char *s, int32_t strlen, int32_t fd)
{
	if (!s)
		return (0);
	return (write(fd, s, strlen));
}

int32_t	ft_output_number_fd(char p[], int32_t fd)
{
	int32_t len;

	len = 0;
	if (!p)
		return (0);
	while (p[len])
		len++;
	return (write(fd, p, len));
}

int32_t	ft_output_str_fd(const char **fmt, int32_t fd)
{
	const char *cur;

	cur = *fmt;
	while (**fmt && **fmt != '%')
		(*fmt)++;
	return (write(fd, cur, *fmt - cur));
}
