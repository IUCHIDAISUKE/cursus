/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversion2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:25:59 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 16:07:20 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_conversion.h"
#include "../../include/ft_output.h"
#include "../../include/ft_utils.h"

int32_t	ft_conversion_percent(void)
{
	int32_t len;

	len = 0;
	if (g_data.flag & MINUS)
	{
		len += ft_putchar_fd('%', STDOUT_);
		len += ft_output_blank_fd(g_data.wid - 1, ' ', STDOUT_);
	}
	else
	{
		if (g_data.wid)
		{
			len += ((g_data.flag & ZERO)
						? (ft_output_blank_fd(g_data.wid - 1, '0', STDOUT_))
						: (ft_output_blank_fd(g_data.wid - 1, ' ', STDOUT_)));
		}
		len += ft_putchar_fd('%', STDOUT_);
	}
	return (len);
}

int32_t	ft_conversion_decimal_sub(char *p)
{
	int32_t space;
	int32_t len;

	len = 0;
	if (g_data.prec == -1 && (g_data.flag & ZERO))
	{
		if (g_data.num < 0)
			len += ft_putchar_fd('-', STDOUT_);
		space = g_data.wid - g_data.digit - (g_data.num < 0);
		len += ft_output_blank_fd(space, '0', STDOUT_);
	}
	else
	{
		space = g_data.wid - (g_data.num < 0) -
											ft_max(g_data.prec, g_data.digit);
		if (g_data.prec == -1 && (g_data.flag & ZERO))
			len += ft_output_blank_fd(space, '0', STDOUT_);
		else
			len += ft_output_blank_fd(space, ' ', STDOUT_);
		if (g_data.num < 0)
			len += ft_putchar_fd('-', STDOUT_);
		len += ft_output_blank_fd(g_data.prec - g_data.digit, '0', STDOUT_);
	}
	len += ft_output_number_fd(p, STDOUT_);
	return (len);
}

int32_t	ft_conversion_decimal(va_list *ap, bool unsign)
{
	char	p[1200];
	int64_t	num;
	int32_t	len;

	len = 0;
	if (unsign)
		num = va_arg(*ap, uint32_t);
	else
		num = va_arg(*ap, int32_t);
	ft_decimal(num, p);
	if (g_data.prec == 0 && num == 0)
		len += ft_output_blank_fd(g_data.wid, ' ', STDOUT_);
	else if (g_data.flag & MINUS)
	{
		if (g_data.num < 0)
			len += ft_putchar_fd('-', STDOUT_);
		len += ft_output_blank_fd(g_data.prec - g_data.digit, '0', STDOUT_);
		len += ft_output_number_fd(p, STDOUT_);
		len += ft_output_blank_fd(g_data.wid - (g_data.num < 0) -
				ft_max(g_data.prec, g_data.digit), ' ', STDOUT_);
	}
	else
		len += ft_conversion_decimal_sub(p);
	return (len);
}

int32_t	ft_conversion_hexadecimal_sub(char *p)
{
	int32_t len;
	int32_t space;

	len = 0;
	if (g_data.prec == -1 && (g_data.flag & ZERO))
		len += ft_output_blank_fd(g_data.wid - g_data.digit -
						(g_data.num < 0), '0', STDOUT_);
	else
	{
		space = g_data.wid - ft_max(g_data.prec, g_data.digit);
		if (g_data.prec == -1 && (g_data.flag & ZERO))
			len += ft_output_blank_fd(space, '0', STDOUT_);
		else
			len += ft_output_blank_fd(space, ' ', STDOUT_);
		len += ft_output_blank_fd(g_data.prec - g_data.digit, '0', STDOUT_);
	}
	len += ft_output_number_fd(p, STDOUT_);
	return (len);
}

int32_t	ft_conversion_hexadecimal(va_list *ap, bool upper)
{
	char	p[1200];
	int32_t	len;

	len = 0;
	ft_hexadecimal(va_arg(*ap, uint32_t), upper, 0, p);
	if (g_data.prec == 0 && g_data.num == 0)
		len += ft_output_blank_fd(g_data.wid, ' ', STDOUT_);
	else if (g_data.flag & MINUS)
	{
		len += ft_output_blank_fd(g_data.prec - g_data.digit, '0', STDOUT_);
		len += ft_output_number_fd(p, STDOUT_);
		len += ft_output_blank_fd(g_data.wid -
					ft_max(g_data.prec, g_data.digit), ' ', STDOUT_);
	}
	else
		len += ft_conversion_hexadecimal_sub(p);
	return (len);
}
