/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conversion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:27:23 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 16:05:25 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_conversion.h"
#include "../../include/ft_output.h"

int32_t	ft_conversion_c(va_list *ap)
{
	int32_t c;
	int32_t len;

	len = 0;
	c = (unsigned char)va_arg(*ap, int32_t);
	if (g_data.flag & MINUS)
	{
		len += ft_putchar_fd(c, STDOUT_);
		if (g_data.prec && g_data.prec != -1)
			len += ft_output_blank_fd(g_data.prec - 1, ' ', STDOUT_);
		else
			len += ft_output_blank_fd(g_data.wid - 1, ' ', STDOUT_);
	}
	else
	{
		len += ((g_data.flag & ZERO)
					? (ft_output_blank_fd(g_data.wid - 1, '0', STDOUT_))
					: (ft_output_blank_fd(g_data.wid - 1, ' ', STDOUT_)));
		len += ft_putchar_fd(c, STDOUT_);
	}
	return (len);
}

int32_t	ft_conversion_str(va_list *ap)
{
	char	*str;
	int32_t	len;
	int32_t	strlen;

	len = 0;
	str = va_arg(*ap, char *);
	if (!str)
		str = (char *)"(null)";
	strlen = ((g_data.prec == -1 || (int64_t)ft_strlen(str) < g_data.prec)
					? ft_strlen(str) : g_data.prec);
	if (g_data.flag & MINUS)
	{
		len += ft_output_lenstr_fd(str, strlen, STDOUT_);
		len += ft_output_blank_fd(g_data.wid - strlen, ' ', STDOUT_);
	}
	else
	{
		len += ((g_data.flag & ZERO)
					? (ft_output_blank_fd(g_data.wid - strlen, '0', STDOUT_))
					: (ft_output_blank_fd(g_data.wid - strlen, ' ', STDOUT_)));
		len += ft_output_lenstr_fd(str, strlen, STDOUT_);
	}
	return (len);
}

int32_t	ft_conversion_ptr_zero(void)
{
	int32_t len;

	len = 0;
	if (g_data.flag & MINUS)
	{
		len += ft_output_lenstr_fd("0x", 2, STDOUT_);
		len += ft_output_blank_fd(g_data.wid - 2, ' ', STDOUT_);
	}
	else
	{
		len += ft_output_blank_fd(g_data.wid - 2, ' ', STDOUT_);
		len += ft_output_lenstr_fd("0x", 2, STDOUT_);
	}
	return (len);
}

int32_t	ft_conversion_ptr(va_list *ap)
{
	char		p[1200];
	uintptr_t	ptr;
	int32_t		len;

	len = 0;
	ptr = va_arg(*ap, uintptr_t);
	ft_hexadecimal(ptr, 0, 1, p);
	if (g_data.prec == 0 && g_data.num == 0)
		len += ft_conversion_ptr_zero();
	else if (g_data.flag & MINUS)
	{
		len += ft_output_lenstr_fd("0x", 2, STDOUT_);
		len += ft_output_blank_fd(g_data.prec - g_data.digit - 2, '0', STDOUT_);
		len += ft_output_number_fd(p, STDOUT_);
		len += ft_output_blank_fd(g_data.wid - 2 - g_data.digit, ' ', STDOUT_);
	}
	else
	{
		len += ft_output_blank_fd(g_data.wid - g_data.digit - 2, ' ', STDOUT_);
		len += ft_output_lenstr_fd("0x", 2, STDOUT_);
		len += ft_output_blank_fd(g_data.prec - g_data.digit, '0', STDOUT_);
		len += ft_output_number_fd(p, STDOUT_);
	}
	return (len);
}
