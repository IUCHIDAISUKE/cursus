/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_radix.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:22:40 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:23:35 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_radix.h"

void	ft_decimal(int64_t n, char *p)
{
	int32_t digit;
	int64_t tmp;

	digit = 1;
	tmp = n;
	g_data.num = n;
	while (n /= 10)
		digit++;
	g_data.digit = digit;
	*(p + digit--) = '\0';
	if (tmp == 0)
		*p = '0';
	if (tmp < 0)
		tmp *= -1;
	while (tmp)
	{
		*(p + digit--) = RADIX[tmp % 10];
		tmp /= 10;
	}
}

void	ft_hexadecimal(uint64_t n, bool upper, bool ptr, char *p)
{
	int32_t		digit;
	uint64_t	tmp;

	digit = 1;
	tmp = (ptr ? n : (uint32_t)n);
	g_data.num = n;
	while (tmp /= 16)
		digit++;
	g_data.digit = digit;
	*(p + digit--) = '\0';
	tmp = (ptr ? n : (uint32_t)n);
	if (tmp == 0)
		*p = '0';
	while (tmp)
	{
		*(p + digit--) = RADIX[tmp % 16] - ((upper && tmp % 16 >= 10) ? 32 : 0);
		tmp /= 16;
	}
}

void	ft_pointer(void *vptr, char *p)
{
	uintptr_t ptr;

	ptr = (uintptr_t)vptr;
	ft_hexadecimal(ptr, 0, 1, p);
}
