/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_struct.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 13:58:30 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 16:02:40 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_struct.h"

void	ft_init(t_data *g_data)
{
	g_data->flag = 0;
	g_data->conv = 0;
	g_data->wid = -1;
	g_data->prec = -1;
	g_data->p[0] = '\0';
	g_data->num = 0;
	g_data->digit = 0;
}
