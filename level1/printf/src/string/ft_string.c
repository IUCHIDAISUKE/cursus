/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_string.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 03:23:55 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 03:24:28 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/ft_string.h"

int32_t	ft_strpos(const char *str, char c)
{
	const char *tmp;

	if (!str || !c)
		return (0);
	tmp = str;
	while (*str)
	{
		if (*str == c)
			return (str - tmp + 1);
		str++;
	}
	return (0);
}
