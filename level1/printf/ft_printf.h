/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/06 17:53:34 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 16:00:56 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "include/ft_conversion.h"
# include "include/ft_output.h"
# include "include/ft_radix.h"
# include "include/ft_struct.h"
# include "include/ft_string.h"
# include "include/ft_utils.h"
# include "include/libft.h"
# include <stdarg.h>
# include <unistd.h>
# include <stdint.h>

# define FLAG "-0"
# define CONV "cspdiuxX%"
# define NO_CONV 0
# define MINUS 0x01
# define ZERO 0x02
# define PRE_MINUS 0x04

typedef union u_var	t_var;
union	u_var
{
	char			a;
	int32_t			b;
	char			*str;
	uintptr_t		ptr;
};

typedef enum
{
	STDERR_ = -1,
	STDIN_,
	STDOUT_,
}	t_std;

t_data	g_data;
int		ft_printf(const char *fmt, ...);

#endif
