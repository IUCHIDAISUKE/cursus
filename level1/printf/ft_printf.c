/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/27 15:57:24 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/27 19:36:08 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int32_t	ft_output(va_list *ap, const char **fmt)
{
	(*fmt)++;
	if (g_data.conv & (1 << 1))
		return (ft_conversion_c(ap));
	if (g_data.conv & (1 << 2))
		return (ft_conversion_str(ap));
	if (g_data.conv & (1 << 3))
		return (ft_conversion_ptr(ap));
	if ((g_data.conv & (1 << 4)) || (g_data.conv & (1 << 5)) ||
		(g_data.conv & (1 << 6)))
		return (ft_conversion_decimal(ap, g_data.conv & (1 << 6)));
	if ((g_data.conv & (1 << 7)) || (g_data.conv & (1 << 8)))
		return (ft_conversion_hexadecimal(ap, g_data.conv & (1 << 8)));
	if (g_data.conv & (1 << 9))
		return (ft_conversion_percent());
	return (-1);
}

int64_t	ft_setnum(va_list *ap, const char **fmt, bool dot)
{
	int64_t num;

	num = -1;
	if (**fmt == '*')
	{
		num = (int)va_arg(*ap, int);
		if ((num < 0) && dot)
			num = -1;
		else if (num < 0)
		{
			num *= -1;
			g_data.flag = MINUS;
		}
		(*fmt)++;
	}
	else if (ft_isdigit(**fmt) || dot)
	{
		if (**fmt == '-')
			return (-1);
		num = 0;
		while (ft_isdigit(**fmt))
			num = 10 * num + (*(*fmt)++ - '0');
	}
	return (num);
}

void	ft_set_data(va_list *ap, const char **fmt)
{
	int32_t index;

	ft_init(&g_data);
	(*fmt)++;
	while ((index = ft_strpos(FLAG, **fmt)))
	{
		if (index == 1)
			g_data.flag |= MINUS;
		else
			g_data.flag |= ZERO;
		(*fmt)++;
	}
	if (g_data.flag & MINUS)
		g_data.flag &= ~(ZERO);
	g_data.wid = ft_setnum(ap, fmt, 0);
	if (**fmt == '.')
	{
		(*fmt)++;
		g_data.prec = ft_setnum(ap, fmt, 1);
	}
	g_data.conv |= (1 << ft_strpos(CONV, **fmt));
}

int		ft_printf(const char *fmt, ...)
{
	va_list	ap;
	int		len;
	int		res;

	res = 0;
	if (!fmt)
		return (-1);
	va_start(ap, fmt);
	while (*fmt)
	{
		if (*fmt != '%')
			res += ft_output_str_fd(&fmt, STDOUT_);
		else
		{
			ft_set_data(&ap, &fmt);
			if (g_data.wid >= INT32_MAX || g_data.prec > INT32_MAX)
				return (-1);
			len = ft_output(&ap, &fmt);
			res = ((len < 0) ? -1 : res + len);
		}
		if (res == -1)
			return (-1);
	}
	va_end(ap);
	return (res);
}
