/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/28 23:30:17 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/12 12:29:54 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*freesult(char *buff, char *ans)
{
	free(buff);
	return (ans);
}

char	*ft_strext(char *str, int st, int size)
{
	char	*res;
	int		cur;

	if (!(res = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	cur = 0;
	while (cur < size)
	{
		*(res + cur) = *(str + st + cur);
		cur++;
	}
	*(res + cur) = '\0';
	if (st)
		free(str);
	return (res);
}

char	*ft_strjoin(char **store, char *buff)
{
	char	*res;
	char	*tmp;
	int		len;
	int		i;

	i = 0;
	tmp = *store;
	len = ft_strlen(*store) + ft_strlen(buff);
	if (!(res = (char *)malloc(sizeof(char) * (len + 1))))
	{
		if (*store && buff)
			free(*store);
		return (NULL);
	}
	while (tmp && *tmp)
		res[i++] = *tmp++;
	while (buff && *buff)
		res[i++] = *buff++;
	res[i] = '\0';
	if (*store && buff)
		free(*store);
	return (res);
}

char *ft_reader(int fd, int *ret, char **store, ssize_t l_read)
{
	char	*buff;
	char	*res;
	int		pos;

	res = NULL;
	if (!(buff = (char *)malloc(sizeof(char) * (BUFFER_SIZE + 1))))
		return (NULL);
	while ((pos = ft_charpos(*store, CHAR_NEW)) == -1 &&
		(l_read = read(fd, buff, BUFFER_SIZE)) > 0)
	{
		buff[l_read] = '\0';
		if (!(*store = ft_strjoin(store, buff)))
			return (freesult(buff, NULL));
	}
	if (pos > -1 && l_read > 0)
	{
		if ((res = ft_strext(*store, 0, pos)) &&
		((*store = ft_strext(*store, pos + 1, ft_strlen(*store) - pos - 1))))
			*ret = SUCCESSS_;
	}
	else if (pos == -1 && l_read == 0)
		if (!(res = ft_strjoin(store, NULL)))
			return (freesult(buff, NULL));
	*ret = ((pos == -1 && l_read == 0) ? EOF_ : *ret);
	return (freesult(buff, res));
}


int		get_next_line(int fd, char **line)
{
	int			ret;
	static char	*store;

	if (fd < 0 || BUFFER_SIZE <= 0 || !line)
		return (ERROR_);
	if (read(fd, store, 0) == -1)
		return (ERROR_);
	ret = ERROR_;
	if (!(*line = ft_reader(fd, &ret, &store, 1)))
	{
		if (store)
			free(store);
		store = NULL;
		ret = ERROR_;
	}
	else if (!ret)
	{
		free(store);
		store = NULL;
	}
	return (ret);
}
