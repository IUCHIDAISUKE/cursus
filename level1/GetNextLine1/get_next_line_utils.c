/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 22:23:38 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/12 12:29:59 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int	ft_strlen(char *str)
{
	int l;

	l = 0;
	if (!str)
		return (0);
	while (str[l])
		l++;
	return (l);
}

int	ft_charpos(char *str, char c)
{
	char *tmp;

	if (!str || !c)
		return (-1);
	tmp = str;
	while (*str)
	{
		if (*str == c)
			return (str - tmp);
		str++;
	}
	return (-1);
}
