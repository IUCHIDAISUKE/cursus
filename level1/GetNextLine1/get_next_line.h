/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: diuchi <diuchi@student.42tokyo.jp>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 10:41:34 by diuchi            #+#    #+#             */
/*   Updated: 2021/02/12 18:38:51 by diuchi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>
# define CHAR_NEW '\n'

int		ft_strlen(char *str);
int		ft_charpos(char *str, char c);
char	*freesult(char *buff, char *ans);
char	*ft_strext(char *str, int st, int size);
char	*ft_strjoin(char **store, char *buff);
char	*ft_reader(int fd, int *ret, char **store, ssize_t l_read);
int		get_next_line(int fd, char **line);

typedef enum
{
	ERROR_ = -1,
	EOF_,
	SUCCESSS_,
}	t_result;

#endif
